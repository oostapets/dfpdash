<?php
class HtmlHelper{

	private function getAdvertiserName($id, $advertisers){
		foreach ($advertisers as $advertiser){
			if ($advertiser->id == $id) {
				return $advertiser->name;
			}
		}
		return "";
	}

	private function showPaginator($totalResults){
		if (isset($_GET['page'])){
			$currentPage = $_GET['page'];
		}else{
			$currentPage = 1;
		}

		$result = '<div class="paginator"><a href="?page=1">&laquo;</a>';

		$maxPage = ceil($totalResults/ITEMS_ON_PAGE);
		$paginatorOffset = 5;
		if (($currentPage - 1) > $paginatorOffset){
			$from = $currentPage - $paginatorOffset;
			$result .= "...";
		}else{
			$from = 1;
		}
		if (($currentPage + $paginatorOffset + 1) < $maxPage){
			$to = $currentPage + $paginatorOffset;
		}else{
			$to = $maxPage;
		}

		for ($i=$from; $i<=$to; $i++){
			if($currentPage == $i){
				$active = "active";
			}else{
				$active = "";
			}
			$result .= '<a href="?page=' . $i . '" class="' . $active . '">' . $i . '</a>';
		}

		if ($to != $maxPage) {
			$result .= "...";
		}

		$result .= '<a href="?page=' . $maxPage . '">&raquo;</a></div>';

		return $result;
	}

	function showOrders($data){
		$result = '
		<h4>Orders: ' . $data['total_results'] . '</h4>
		<table>
		<tr>
			<th class="align_left">Name</th>
			<th class="align_left">Advertiser</th>
			<th class="align_left">Start time</th>
			<th class="align_left">End time</th>
			<th class="align_left">Total projected value</th>
			<th class="align_left">Impressions</th>
			<th class="align_left">Clicks</th>
			<th class="align_left">Notes</th>
		</tr>
		';

		foreach ($data['orders'] as $order) {
			$start_datetime = DateTimeUtils::ToStringWithTimeZone($order->startDateTime);
			$converted_start_datetime = date('M j, Y', strtotime($start_datetime)) . '<br />' . date('h:i A T', strtotime($start_datetime));

			if (!$order->unlimitedEndDateTime){
				$end_datetime = DateTimeUtils::ToStringWithTimeZone($order->endDateTime);
				$converted_end_datetime = date('M j, Y', strtotime($end_datetime)) . '<br />' . date('h:i A T', strtotime($end_datetime));
			}else{
				$converted_end_datetime = "Unlimited";
			}

			setlocale(LC_MONETARY, 'en_CA');
			$budget = $order->totalBudget->currencyCode.(money_format('%i',($order->totalBudget->microAmount / 1000000)));

			$result .= '
			<tr>
				<td class="align_left"><a href="?p=OrderDetails&orderId=' . $order->id . '">' . $order->name . '</a><br />ID: ' . $order->id . '</td>
				<td class="align_left">' . $this->getAdvertiserName($order->advertiserId, $data['companies']) . '</td>
				<td class="align_right">' . $converted_start_datetime . '</td>
				<td class="align_right">' . $converted_end_datetime . '</td>
				<td class="align_right">' . $budget . '</td>
				<td class="align_right">' . number_format($order->totalImpressionsDelivered) . '</td>
				<td class="align_right">' . number_format($order->totalClicksDelivered) . '</td>
				<td class="align_left">' . $order->notes . '</td>
			</tr>
			';
		}

		$result .= '
		</table>
		';

		$result .= $this->showPaginator($data['total_results']);

		return $result;
	}

	function showOrderDetails($data){
		$order = $data['order'][0];

		$impressions = 0;
		$clicks = 0;
		foreach ($data['items'] as $item){
			$impressions += $item->stats->impressionsDelivered;
			$clicks += $item->stats->clicksDelivered;
		}

		$result = '
		<h4>Order: ' . $order->name . '</h4>
		<table class="descriptor">
		<tr>
			<td>Impressions</td>
			<td>Clicks</td>
			<td>CTR</td>
		</tr>
		<tr>
			<td class="big">' . $impressions . '</td>
			<td class="big">' . $clicks . '</td>
			<td class="big last">' . $this->calcCTR($clicks, $impressions) . '%</td>
		</tr>
		</table>
		<table>
		<tr>
			<th class="align_left">Name</th>
			<th class="align_left">Status</th>
			<th class="align_left">Type</th>
			<th class="align_left">Priority</th>
			<th class="align_left">Target platform</th>
			<th class="align_left">Start time</th>
			<th class="align_left">End time</th>
			<th class="align_left">Progress</th>
			<th class="align_left">Rate</th>
			<th class="align_left">Goal</th>
			<th class="align_left">Impressions</th>
			<th class="align_left">Clicks</th>
			<th class="align_left">CTR</th>
			<th class="align_left">Comments</th>
		</tr>
		';
//echo "<pre>";var_dump($data['items']);exit;
		foreach ($data['items'] as $item) {
			$placeholders = $this->getPlaceholders($item->creativePlaceholders);

			$start_datetime = DateTimeUtils::ToStringWithTimeZone($item->startDateTime);
			$converted_start_datetime = date('M j, Y', strtotime($start_datetime)) . '<br />' . date('h:i A T', strtotime($start_datetime));

			if (!$item->unlimitedEndDateTime){
				$end_datetime = DateTimeUtils::ToStringWithTimeZone($item->endDateTime);
				$converted_end_datetime = date('M j, Y', strtotime($end_datetime)) . '<br />' . date('h:i A T', strtotime($end_datetime));
			}else{
				$converted_end_datetime = "Unlimited";
			}

			setlocale(LC_MONETARY, 'en_CA');
			$budget = $item->budget->currencyCode.(money_format('%i',($item->budget->microAmount / 1000000)));

			$result .= '
			<tr>
				<td class="align_left">' . $item->name . '<br />ID: ' . $item->id . '<br />' . $placeholders . '</td>
				<td class="align_left">' . ucfirst(strtolower($item->status)) . '</td>
				<td class="align_left">' . ucfirst(strtolower($item->lineItemType)) . '</td>
				<td class="align_right">' . $item->priority . '</td>
				<td class="align_left">' . ucfirst(strtolower($item->targetPlatform)) . '</td>
				<td class="align_right">' . $converted_start_datetime . '</td>
				<td class="align_right">' . $converted_end_datetime . '</td>
				<td class="align_right">' . $this->showProgressIndicator($item) . '</td>
				<td class="align_right">' . $budget . '</td>
				<td class="align_right">' . $this->showGoal($item) . '</td>
				<td class="align_right">' . number_format($item->stats->impressionsDelivered) . '</td>
				<td class="align_right">' . number_format($item->stats->clicksDelivered) . '</td>
				<td class="align_right">' . $this->calcCTR($item->stats->clicksDelivered, $item->stats->impressionsDelivered) . '%</td>
				<td class="align_right">' . $item->notes . '</td>
			</tr>
			';
		}

		$result .= '
		</table>
		';

		return $result;
	}

	private function showProgressIndicator($item){
		$indicator = $item->deliveryIndicator;
		$data = $item->deliveryData;
//var_dump($indicator);exit;
		if(!$indicator && !$data){
			$result = "N/A";
		}elseif($indicator){
			$result = '
			<div class="deliveryIndicator1">
				<div class="deliveryIndicator2" style="width:' . floor($indicator->actualDeliveryPercentage) . '%">
					' . floor(($indicator->actualDeliveryPercentage / $indicator->expectedDeliveryPercentage) * 100) . '%
				</div>
			</div>
			';
		}else{
			$units = $data->units;
			$max_unit = 0;
			foreach ($units as $unit){
				if ($unit > $max_unit){
					$max_unit = $unit;
				}
			}

			$result = '
			<div class="deliveryProgress1">
				<div class="deliveryProgress2">
					<div class="deliveryProgress3" id="chart_div' . $item->id . '"></div>
				</div>' . number_format($max_unit) . '
			</div>

			<script type="text/javascript">
				google.setOnLoadCallback(drawStuff);

				function drawStuff() {
					var data = new google.visualization.arrayToDataTable([
						["", "Percentage"],
						[" ", ' . $units[0] . '],
						[" ", ' . $units[1] . '],
						[" ", ' . $units[2] . '],
						[" ", ' . $units[3] . '],
						[" ", ' . $units[4] . '],
						[" ", ' . $units[5] . '],
						[" ", ' . $units[6] . ']
						]);

					var options = {
						backgroundColor: "#E8F7FF",
						width: 157,
						height: 35,
						legend: { position: "none" },
						chart: { },
						bar: { groupWidth: "100%" }
					};

					var chart = new google.charts.Bar(document.getElementById("chart_div' . $item->id . '"));

					chart.draw(data, google.charts.Bar.convertOptions(options));
				};
			</script>
			';
		}
		return $result;
	}

	private function showGoal($item){
		if($item->primaryGoal->goalType == 'DAILY'){
			if($item->lineItemType == 'SPONSORSHIP'){
				$goalText = 'Total impressions';
			}else{
				$goalText = 'Remaining impressions';
			}
			$result = $item->primaryGoal->units . '%<br />' . $goalText;
		}else{
			$result = number_format($item->primaryGoal->units) . '<br />' . ucfirst(strtolower($item->primaryGoal->unitType));
		}
		return $result;
	}

	private function calcCTR($clicks, $impressions){
		return round(($clicks / $impressions) * 100, 2);
	}

	private function getPlaceholders($placeholders){
		$result = '';
		foreach ($placeholders as $placeholder){
			if($placeholder->size->width != 1) {
				if($result == ''){
					$result = $placeholder->size->width . 'x' . $placeholder->size->height;
				}else{
					$result .= ', ' . $placeholder->size->width . 'x' . $placeholder->size->height;
				}
			}
		}
		if($result == ''){
			$result = 'Out-of-page';
		}
		return $result;
	}
}