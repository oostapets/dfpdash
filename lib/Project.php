<?php

date_default_timezone_set( 'America/Winnipeg' );

class Project{

	public function __construct(){
	}

	function getCurrentURL(){
		$pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
		if ($_SERVER["SERVER_PORT"] != "80")
		{
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		}
		else
		{
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		$p = explode('?',$pageURL);
		return $p[0];
	}

	function getBaseURL(){
		return "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER["REQUEST_URI"].'?').'/';
	}

	function redirect($url, $permanent = false) {
		if (headers_sent() === false){
			header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
		}

		exit();
	}



	function getAllOrders(){
		try {
			$user = new DfpUser();
			$user->LogDefaults();

			$orderService = $user->GetService('OrderService', 'v201502');

			$statementBuilder = new StatementBuilder();
			$statementBuilder->OrderBy('name DESC')
				->Limit(ITEMS_ON_PAGE);
			if (isset($_GET['page'])){
				$currentPage = $_GET['page'];
			}else{
				$currentPage = 1;
			}
			$offset = ($currentPage - 1) * ITEMS_ON_PAGE;
			$statementBuilder->IncreaseOffsetBy($offset);

			$page = $orderService->getOrdersByStatement(
				$statementBuilder->ToStatement());

			if (isset($page->results)) {
				$companyService = $user->GetService('CompanyService', 'v201502');

				// Getting Advertiser's names
				// Ugly code because DFP doesn't support complicated queries
				$ids = "(";
				foreach ($page->results as $order) {
					if ($ids == "(") {
						$ids .= $order->advertiserId;
					}else{
						$ids .= "," . $order->advertiserId;
					}
				}
				$ids .= ")";
				$statementBuilder
					->Where('id IN ' . $ids)
					->OrderBy('id ASC')
					->Limit(9999);
				$rcompany = $companyService->getCompaniesByStatement(
					$statementBuilder->ToStatement());
				return array("orders" => $page->results, "companies" => $rcompany->results, "total_results" => $page->totalResultSetSize);
			}

		} catch (OAuth2Exception $e) {
			ExampleUtils::CheckForOAuth2Errors($e);
		} catch (ValidationException $e) {
			ExampleUtils::CheckForOAuth2Errors($e);
		} catch (Exception $e) {
			printf("%s\n", $e->getMessage());
		}
	}

	function getOrderDetails(){
		try {
			$user = new DfpUser();
			$user->LogDefaults();

			$lineItemService = $user->GetService('LineItemService', 'v201502');

			$statementBuilder = new StatementBuilder();
			$statementBuilder->OrderBy('status DESC')
				->Where('orderId = :orderId')
				->WithBindVariableValue('orderId', $_GET['orderId'])
				->Limit(99999);

			$page = $lineItemService->getLineItemsByStatement(
				$statementBuilder->ToStatement());

			if (isset($page->results)) {
				$orderService = $user->GetService('OrderService', 'v201502');
				$statementBuilder = new StatementBuilder();
				$statementBuilder
					->Where('id = :orderId')
					->WithBindVariableValue('orderId', $_GET['orderId']);

				$order = $orderService->getOrdersByStatement(
					$statementBuilder->ToStatement());

				return array("items" => $page->results, "order" => $order->results);
			}
		} catch (OAuth2Exception $e) {
			ExampleUtils::CheckForOAuth2Errors($e);
		} catch (ValidationException $e) {
			ExampleUtils::CheckForOAuth2Errors($e);
		} catch (Exception $e) {
			printf("%s\n", $e->getMessage());
		}
	}
}
