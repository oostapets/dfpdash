<?php
$project = new Project();

$template = '
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
	<title>{title/}</title>
    <link rel="stylesheet" href="' . $project->getBaseURL() . 'css/styles.css">
    {css/}
    {javascript/}
</head>

<body id="home">

	<div data-role="page" id="page-home" data-theme="a" style="display:table" >

		<div data-role="content" id="content" class="ui-content">

			{content/}

		</div>

	</div>
</body>
</html>
';

function showTemplate($params){
	global $template;

	foreach ($params as $key => $value) {
		$template = str_replace("{" . $key . "/}", $value, $template);
	}
	echo $template;
}

