<?php
if ( ! defined( 'ITEMS_ON_PAGE' ) ) define( 'ITEMS_ON_PAGE', 10 );
//require_once('examples/Dfp/Auth/GetRefreshToken.php');
//include('examples/Dfp/v201502/OrderService/GetAllOrders.php');

error_reporting(E_STRICT | E_ALL);

$path = dirname(__FILE__) . '/src';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

require_once 'Google/Api/Ads/Dfp/Lib/DfpUser.php';
require_once 'Google/Api/Ads/Dfp/Util/StatementBuilder.php';
require_once dirname(__FILE__) . '/examples/Common/ExampleUtils.php';

include('lib/Project.php');
include('lib/HtmlHelper.php');
include('template/index.php');

$project = new Project();
$htmlHelper = new HtmlHelper();

if(isset($_GET['p']) and !empty($_GET['p'])) {
	$p = $_GET['p'];
}else{
	$p = '';
}
switch ($p) {
	case "OrderDetails":
		$data = $project->getOrderDetails();
		$content = $htmlHelper->showOrderDetails($data);

		showTemplate(array(
			'title' => 'Order Details',
			'css' => '',
			'javascript' => '<script type="text/javascript" src="https://www.google.com/jsapi"></script>
							<script type="text/javascript">
								google.load("visualization", "1.1", {packages:["bar"]});
							</script>',
			'content' => $content
		));
		break;

	default:
		$data = $project->getAllOrders();
		$content = $htmlHelper->showOrders($data);

		showTemplate(array(
			'title' => 'Orders',
			'css' => '',
			'javascript' => '',
			'content' => $content
		));
		break;
}
